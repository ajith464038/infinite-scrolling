const container = document.querySelector(".container-section");

async function loadNextSet() {
  for (let i = 0; i < 8; i++) {
    const catCard = document.createElement("div");
    catCard.classList.add("card");
    catCard.classList.add("mb-3");
    catCard.classList.add("skeletonCard");
    catCard.innerHTML = `
    <div class="row g-0">
        <div class="col-md-4">
        <div style="height: 150px; width: 150px" class="skeleton"></div>

    </div>
    <div class="col-md-8">
        <div class="card-body">
            <p class="card-text skeleton-text skeleton">
            </p>
        </div>
        </div>
    </div>`;
    container.appendChild(catCard);
  }
  for (let i = 0; i < 8; i++) {
    const data = await fetch("https://api.thecatapi.com/v1/images/search")
      .then((res) => {
        if (res.status !== 200) {
          return console.log("request failed");
        }
        return res.json();
      })
      .then((res) => res);
    const facts = await fetch("https://catfact.ninja/fact")
      .then((res) => {
        if (res.status !== 200) {
          return console.log("request failed");
        }
        return res.json();
      })
      .then((res) => res);

    if (Array.from(document.querySelectorAll(".skeletonCard")).length !== 0) {
      document.querySelectorAll(".skeletonCard")[0].innerHTML = `    
      <div class="row g-0">
        <div class="col-md-4">
            <img src="${data[0]["url"]}" style="height: 210px; width: 280px" class="img-fluid rounded-start" loading="lazy" alt="${data[0]["id"]}" />
        </div>
        <div class="col-md-8">
        <div class="card-body">
              <p class="card-text">${facts.fact}
            </p>
        </div>
      </div>
  </div>`;
      document
        .querySelectorAll(".skeletonCard")[0]
        .classList.remove("skeletonCard");
    }
  }
}

loadNextSet();

container.addEventListener("scroll", (e) => {
  const { scrollTop, scrollHeight } = container;
  const height = container.getBoundingClientRect().height;
  if (scrollTop >= scrollHeight - height - 20) {
    loadNextSet();
  }
});
